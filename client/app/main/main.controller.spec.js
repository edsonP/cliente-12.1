'use strict';

describe('Controller: MainCtrl', function () {

    // load the controller's module
    beforeEach(module('proyectoFullstackApp'));

    var vm,
        $httpBackend;

    // Initialize the controller and a mock scope
    beforeEach(inject(function (_$httpBackend_, $controller, $rootScope) {
        $httpBackend = _$httpBackend_;
        $httpBackend.expectGET('/api/cursos')
            .respond({
                "cursos": [
                    {
                        "id": 0,
                        "name": "AngularJS",
                        "state": "main.angular",
                        "inscritos": [
                            {
                                "id": 10, "nombre": "Juan", "apellido": "Perez", "fechaNacimiento": "1990-05-01T00:00:00-04:00"
                            }
                        ]
                    },
                    {
                        "id": 1,
                        "name": "Java SE",
                        "state": "main.java",
                        "inscritos": [
                            {
                                "id": 21,
                                "nombre": "Maria",
                                "apellido": "Rodriguez",
                                "fechaNacimiento": "1995-06-01T00:00:00-04:00"
                            }
                        ]
                    }
                ]
            });

        vm = $controller('MainCtrl');
    }));

    it('should define a value for the title', function () {
        expect(vm.title).not.toBeNull();
        expect(vm.title).not.toBeUndefined();
        expect(vm.title).toBeDefined();
    });

    it('should assign a text as a title', function () {
        expect(vm.title).toMatch(/[A-z]/);
    });

    it('should assign "Lista de inscritos por Curso" as title', function () {
        expect(vm.title).toBe('Lista de inscritos por Curso');
    });

    it('should load a list of "cursos"', function () {
        $httpBackend.flush();
        expect(vm.cursos.length).toBe(2);
    });
});
